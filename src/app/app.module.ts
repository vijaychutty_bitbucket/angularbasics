import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppMediaItemComponent } from './app.media.component';


@NgModule({
  declarations: [
    AppComponent,
    AppMediaItemComponent
  ],
  imports: [
    BrowserModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
